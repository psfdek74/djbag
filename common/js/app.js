var app = {
    run: function () {
        this.scrolTo();
    },
    storage: {
        modal: ''
    },
    scrolTo: function () {
        $('[data-scroll-to]').each(function () {
            var selector = $(this).data('scroll-to');
            $(this).on('click', function () {
                var scrollTop = $(selector).offset().top;
                var body = $("html, body");
                body.stop().animate({scrollTop:scrollTop}, '500', 'swing', function() {});
            });
        });
    },
    heighrToBig: function (selector) {
        if($(selector).length>1){
            var mh = 0;
            $(selector).each(function () {
                var h_block = parseInt($(this).height());
                if(h_block > mh) {
                    mh = h_block;
                };
            });
            $(selector).height(mh);
        }else{
            return false;
        }
    }
};
var wow = new WOW(
    {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
            //console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    }
);

var svg = {

    obj: {
        selector: '._svg',
        hoverOn: false,
        color: false,
        hoverColor: '#fc5035'
    },
    run: function () {

        var selector = arguments[0].selector?arguments[0].selector:this.obj.selector;
       // var hoverOn = typeof(arguments[0].hoverOn)=='boolean' ? arguments[0].hoverOn:this.obj.hoverOn;
       // var hoverColor = arguments[0].hoverColor?arguments[0].hoverColor:this.obj.hoverColor;

        $(selector).each(function(){

            var $img = $(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
            var imgTitle = $img.attr('title') ? $img.attr('title') : false;
            var imgStyle = $img.attr('style') ? $img.attr('style') : false;
            var dataWidth = $img.attr('data-width')==1? $img.attr('data-width') : '0';
            var imgWidth = false;
            if(dataWidth=='1'){
                imgWidth = $img.width();
            }
            $.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }
                if(imgTitle){
                    $svg.find('title').html(imgTitle)
                }
                if(imgWidth){
                    imgStyle = imgStyle?imgStyle:'';
                    imgStyle = imgStyle+'; width:'+imgWidth+'px';
                }
                if(imgStyle){
                    $svg.attr('style', imgStyle)
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
    }
};

var autoOpen ={
    run: function () {
        this.actions.checkHash();
    },
    storage:{
      hash: '',
      item: ''
    },
    actions: {
        checkHash: function () {
            if(window.location.hash.length>0){
                var url = window.location.hash;
                autoOpen.storage.hash = url.substring(url.indexOf('#')+1);
                autoOpen.actions.checkModal();
            }
        },
        checkModal: function () {
            autoOpen.storage.item = $(document).find('[data-item-ar=' + autoOpen.storage.hash + ']')
            if(autoOpen.storage.item.length>0){
                autoOpen.actions.openModal();
            }
        },
        openModal: function () {

            var btn = autoOpen.storage.item.find('._js-more');
            $(document).scrollTop(autoOpen.storage.item.offset().top);
            setTimeout(function () {
                btn.click();
            },500);
        }
    }
};

$(document).ready(function () {
    app.run();
    wow.init();
    svg.run({selector:'._svg'});
    autoOpen.run();
    $('#menu').onePageNav({
        currentClass: 'current',
        changeHash: false,
        scrollSpeed: 750
    });
    $('#slider').slick({
        autoplay: true,
        autoplaySpeed: 4000

    });


    $('._js-send').on('submit', function () {
        var form = $(this);

        $.ajax({
            type:"POST",
            enctype:'application/x-www-form-urlencoded',
            url:form.attr("action"),
            data:form.serialize(),
            success:function (data) {
                console.log('1');
                if (data.status == 'ok') {
                    console.log('2');
                    form.html('<div class="_form-w"><div class="_form-row"><h1 style="margin: 0;font-size: 2em;text-align: center;padding: 15%;">Ваша заявка отправлена!</h1></div></div>');
                    setTimeout(function () {
                        app.storage.modal.close();
                    }, 1500);
                } else {
                }
            }
        });
        return false;
    });


    app.storage.modal = $('._j-modal').remodal();
    app.storage.modal2 = $('#j_desc').remodal();

    app.storage.modal2Data = {};

    app.storage.modal2Active = false;

    app.storage.modal3 = $('#j_video').remodal();
    app.storage.modal3Data = {};
    app.storage.modal3Active = false;
/**/
/***/

    $(document).on('opening', app.storage.modal3, function () {

    });
    $(document).on('opened', app.storage.modal3, function () {

    });
    $(document).on('closing', app.storage.modal3, function (e) {

    });

    $(document).on('closed', app.storage.modal3, function (e) {

    });
/**/
    $(document).on('opening', app.storage.modal2, function () {

        if(app.storage.modal2Active) {
            console.log('opening:modal2');
        }
    });
    $(document).on('opened', app.storage.modal2, function () {
        //console.log('opened');
        if(app.storage.modal2Active){
            window.location.hash = app.storage.modal2Data.hash;
            console.log('opened:modal2');
        }
    });
    $(document).on('closing', app.storage.modal2, function (e) {
        //console.log('closing');
        if(app.storage.modal2Active) {
            console.log('closing:modal2');
        }
    });

    $(document).on('closed', app.storage.modal2, function (e) {
        if(app.storage.modal2Active) {
            window.location.hash = '';
            $(document).scrollTop(app.storage.modal2Data.par.offset().top);
            $('._js-hide').html('');
            $('#_item-desc').html('');
            console.log('closed:modal2');
        }
    });

    $('._js-buy').on('click', function () {
        app.storage.modal2Active = false;
        var elem = $(this);
        var parent = elem.closest('._js-product');
        var name = parent.find('._js-name').html();
        $('._js-hide').html('<input type="hidden" name="tovar" value="'+ name +'"/>');
        app.storage.modal.open();
        return false;
    });

    $('._js-more').on('click', function () {
        app.storage.modal2Active = true;
        var elem = $(this);
        var parent = elem.closest('._js-product');
        var desc = parent.data('item-ar');
        app.storage.modal2Data.hash = parent.data('item-ar');
        app.storage.modal2Data.par = parent;

        $.ajax({
            type:"POST",
            enctype:'application/x-www-form-urlencoded',
            url:'./items.php',
            data:'&item='+desc,
            success:function (data) {
                if (data.status !== false) {
                    $('#_item-desc').html(data.data);
                    app.storage.modal2.open();
                } else {

                }
            }
        });


        return false;
    });


    $('._js-buy-all').on('click', function () {
        var elem = $(this);
        var parent = elem.closest('._js-product');
        var name = parent.find('._js-name').html();
        $('._js-hide').html('');
        app.storage.modal.open();
        return false;
    });

    $(".fbgalery").fancybox({
        type: "image"
    });

    $("._js-opgalery").on('click', function () {
        var gallery = $(this).closest('._js-product').find('._hide-galery').find('._jfb-galer');
        var items = [];
        $.each(gallery, function () {
            var el = $(this);
            var href = el.data('fancybox-href')?el.data('fancybox-href'):el.attr('href');
            items.push({
                href: href
            })
        });

        $.fancybox(items, {
            padding: 0
        });
    });

/*
    $(".ScrollBlock").each(function () {
        var elem = $(this);
        var elemOffset = elem.offset();
        elemOffset = elemOffset.top;
        elem.data("elemOffset",elemOffset);
    });

    $(window).scroll(function () {
        $(".ScrollBlock").each(function (i) {
            var posToTop = $(window).scrollTop();
            var elem = $(this);
            var elemOffset = elem.data("elemOffset");
            var positionTop = posToTop;

            if (posToTop > elemOffset) {
                elem.css({"top":""+positionTop+"px", "opacity":"0.2", "position":"absolute"});
                setTimeout(function(){
                    elem.css("opacity", "1");
                },550);
            } else {
                elem.css({"top":""+elemOffset+"px", "opacity":"1"});
            }
        });
    });*/
    app.heighrToBig('._js-pre-name');
    $('#_scr1-box-sl').slick({
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000
    });
    $('#_partner-slider').slick({
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    if(window.innerWidth>=800){
        $("#menuheader").sticky({topSpacing:0});
    }else{
        $("#menuheader").unstick();
    }


    $(window).resize(function () {
        if(window.innerWidth>=800){
            $("#menuheader").sticky({topSpacing:0});
        }else{
            $("#menuheader").unstick();
        }
    });
    $("._ab-tovar-slider-w").slick();

    mBack();
});
function mBack(){
    var regex = /\/(\w+).html|\//g;
    var m = regex.exec(window.location.pathname)
    if(m[1] != ('/'&&'index'&&undefined)){
        $('body').append('<a title="Возврат на главную страницу!" class="_homeback" href="/"><img class="_svg-on" src="/assets/img/back-arrow.svg"></a>');
        console.log(m[1]);
        svg.run({selector:'._svg-on'});
    }
}



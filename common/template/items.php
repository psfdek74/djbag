<?php
header('Content-Type: application/json');
$items = array(
    "djbkb"=>'<h1>Сумка DJB KB предназначена для переноски и хранения DJ контроллеров.</h1>
        <h2>Видео:</h2>
        <iframe style="display:block;" width="100%" height="400px" src="https://www.youtube.com/embed/yDQtP7lTR6k" frameborder="0" allowfullscreen></iframe>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON600D;</li>
			<li>Эти модели имеют ручки и плечевые ремни;</li>
			<li>Подходят под большинство современных контроллеров размерами до 76х40х11 см;</li>
			<li>Мягкие стенки сумок надежно защитят контроллер от внешних воздействий;</li>
			<li>Сумка имеет большой наружный карман куда без проблем поместятся ноутбук и наушники или какая-нибудь необходимая коммутация;</li>
			<li>Два больших внутренних прозрачных кармана;</li>
			<li>Крышку сумки от контроллера разделяет мягкий съемный мат на липучках с большим карманом для ноутбука;</li>
			<li>Во внутреннем отсеке есть необходимые перегородки, чтобы контроллер меньших размеров надежно был закреплен;</li>
			<li>Также имеются дополнительные липучки для крепления контроллера к сумке;</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DDJ : RX , RB , RR , SB , SB2 , SR , SX , SX2 , SP1 ,T1 ,WEGO , а также TOPAZ TSP-16 , XDJ-AERO , XDJ-R1</li>
          <li>NATIVE INSTRUMENTS : Traktor Kontrol S2 , Traktor Kontrol S4 , Traktor Kontrol S5 , Traktor Kontrol S8 , Traktor Kontrol Z2</li>
          <li>Allen&Heath XONE:DX</li>
          <li>Vestax : VCI-100 , VCI-100 mk2 ,  VCI-300 , VCI-300 mk2 , VCI-380 ,VCI-400 , VCI-400 TR , VCI-400 DJ , VCM-100 , VCM-600 , Typhoon , Typhoon VDJ , Spin , Spin 2 , PBS-4</li>
          <li>Numark : NV , NS 6 , NS 7 , N4 , Mixtrack Pro Dj , Mixtrack Pro II ,  Mixtrack Pro III , Mixtrack Platinum , Mixtrack Quad , DJ2GO , 4TRAK , Mixtrack Edge , iDJ , iDJ II iDJ live II , Party Mix , Total Control , Stelth Control , Omni Control , MixMeister Control</li>
          <li>Denon : MC4000 , MC6000</li>
          <li>Ableton : PUSH , PUSH 2</li>
          <li>Reloop : Beatmix 2 , Beatmix 4</li>
          <li>Stanton : DJC.4 , SCS.4DJ , SC System 3</li>
          <li>M-Audio : Torg Xponent , Trigger Finger Pro NOVATION LAUNCH CONTROL , NOVATION LAUNCH CONTROL Mini , ROLAND DJ-808 и др.</li>
        </ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbkg"=>'<h1>Сумка DJB KG предназначена для переноски и хранения DJ контроллеров.</h1>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет серый;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON600D;</li>
			<li>Размеры контроллера до 76х40х11 см;</li>
			<li>Имеет ручки и плечевые ремни;</li>
			<li>Большой наружный карман куда без проблем поместятся ноутбук , наушники и вся необходимая коммутация;</li>
			<li>Большие внутренние прозрачные карманы;</li>
			<li>Крышку сумки от контроллера разделяет мягкий съемный мат на липучках с большим карманом для ноутбука;</li>
			<li>Во внутреннем отсеке имеются перегородки для надежного крепления контроллеров меньших;</li>
			<li>Имеются дополнительные липучки для крепления контроллера к сумке.</li>
			<li>Мягкие стенки сумки надежно защитят контроллер от внешних воздействий;</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DDJ : RZ , RX , RB , RR , SB , SB2 , SR , SX , SX2 , SP1 ,T1 ,WEGO , а также TOPAZ TSP-16 , XDJ-AERO , XDJ-R1</li>
          <li>NATIVE INSTRUMENTS : Traktor Kontrol S2 , Traktor Kontrol S4 , Traktor Kontrol S5 , Traktor Kontrol S8 , Traktor Kontrol Z2</li>
          <li>Allen&Heath XONE:DX</li>
          <li>Vestax : VCI-100 , VCI-100 mk2 , VCI-300 , VCI-300 mk2 , VCI-380 ,VCI-400 , VCI-400 TR , VCI-400 DJ , VCM-100 , VCM-600 , Typhoon , Typhoon VDJ , Spin , Spin 2 , PBS-4</li>
          <li>Numark : NV , NS 6 , NS 7 , N4 , Mixtrack Pro Dj , Mixtrack Pro II , Mixtrack Pro III , Mixtrack Platinum , Mixtrack Quad , DJ2GO , 4TRAK , Mixtrack Edge , iDJ , iDJ II iDJ live II , Party Mix , Total Control , Stelth Control , Omni Control , MixMeister Control</li>
          <li>Denon : MC4000 , MC6000</li>
          <li>Ableton : PUSH , PUSH 2</li>
          <li>Reloop : Beatmix 2 , Beatmix 4</li>
          <li>Stanton : DJC.4 , SCS.4DJ , SC System 3</li>
          <li>M-Audio : Torg Xponent , Trigger Finger Pro NOVATION LAUNCH CONTROL , NOVATION LAUNCH CONTROL Mini , ROLAND DJ-808 и др.</li>
        </ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbkbw"=>'<h1>Сумка DJB K-Wheels предназначена для переноски и хранения DJ контроллеров.</h1>
        <h2>Описание:</h2>
        <p>Эти модели имеют надежные колесики на подшипниках и удобные ручки для переноса.</p>
         <ul>
			<li>Подходят под большинство современных контроллеров размерами до 76х40х11 см;</li>
			<li>Мягкие стенки сумок надежно защитят контроллер от внешних воздействий;</li>
			<li>Сумка имеет большой наружный карман куда без проблем поместятся ноутбук и наушники или какая-нибудь необходимая коммутация;</li>
			<li>Два больших внутренних прозрачных кармана;</li>
			<li>Крышку сумки от контроллера разделяет мягкий съемный мат на липучках с большим карманом для ноутбука;</li>
			<li>Во внутреннем отсеке есть необходимые перегородки, чтобы контроллер меньших размеров надежно был закреплен;</li>
			<li>Также имеются дополнительные липучки для крепления контроллера к сумке;</li>
        </ul>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный (с черными молниями);</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON600D;</li>
			<li>Размеры контроллера до 76х40х11 см;</li>
			<li>Эти модели имеют надежные колесики на подшипниках и удобные ручки для переноса;</li>
			<li>Большой наружный карман куда без проблем поместятся ноутбук , наушники и вся необходимая коммутация;</li>
			<li>Большие внутренние прозрачные карманы;</li>
			<li>Крышку сумки от контроллера разделяет мягкий съемный мат на липучках с большим карманом для ноутбука;</li>
			<li>Во внутреннем отсеке имеются перегородки для надежного крепления контроллеров меньших;</li>
			<li>Имеются дополнительные липучки для крепления контроллера к сумке.</li>
			<li>Мягкие стенки сумки надежно защитят контроллер от внешних воздействий;</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DDJ : RZ , RX , RB , RR , SB , SB2 , SR , SX , SX2 , SP1 ,T1 ,WEGO , а также TOPAZ TSP-16 , XDJ-AERO , XDJ-R1</li>
          <li>NATIVE INSTRUMENTS : Traktor Kontrol S2 , Traktor Kontrol S4 , Traktor Kontrol S5 , Traktor Kontrol S8 , Traktor Kontrol Z2</li>
          <li>Allen&Heath XONE:DX</li>
          <li>Vestax : VCI-100 , VCI-100 mk2 , VCI-300 , VCI-300 mk2 , VCI-380 ,VCI-400 , VCI-400 TR , VCI-400 DJ , VCM-100 , VCM-600 , Typhoon , Typhoon VDJ , Spin , Spin 2 , PBS-4</li>
          <li>Numark : NV , NS 6 , NS 7 , N4 , Mixtrack Pro Dj , Mixtrack Pro II , Mixtrack Pro III , Mixtrack Platinum , Mixtrack Quad , DJ2GO , 4TRAK , Mixtrack Edge , iDJ , iDJ II iDJ live II , Party Mix , Total Control , Stelth Control , Omni Control , MixMeister Control</li>
          <li>Denon : MC4000 , MC6000</li>
          <li>Ableton : PUSH , PUSH 2</li>
          <li>Reloop : Beatmix 2 , Beatmix 4</li>
          <li>Stanton : DJC.4 , SCS.4DJ , SC System 3</li>
          <li>M-Audio : Torg Xponent , Trigger Finger Pro NOVATION LAUNCH CONTROL , NOVATION LAUNCH CONTROL Mini , ROLAND DJ-808 и др.</li>
        </ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbkgw"=>'<h1>Сумка DJB KG W предназначена для переноски и хранения DJ контроллеров.</h1>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет серый;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON600D;</li>
			<li>Размеры контроллера до 76х40х11 см;</li>
			<li>Эти модели имеют надежные колесики на подшипниках и удобные ручки для переноса;</li>
			<li>Большой наружный карман куда без проблем поместятся ноутбук , наушники и вся необходимая коммутация;</li>
			<li>Большие внутренние прозрачные карманы;</li>
			<li>Крышку сумки от контроллера разделяет мягкий съемный мат на липучках с большим карманом для ноутбука;</li>
			<li>Во внутреннем отсеке имеются перегородки для надежного крепления контроллеров меньших;</li>
			<li>Имеются дополнительные липучки для крепления контроллера к сумке.</li>
			<li>Мягкие стенки сумки надежно защитят контроллер от внешних воздействий;</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DDJ : RZ , RX , RB , RR , SB , SB2 , SR , SX , SX2 , SP1 ,T1 ,WEGO , а также TOPAZ TSP-16 , XDJ-AERO , XDJ-R1</li>
          <li>NATIVE INSTRUMENTS : Traktor Kontrol S2 , Traktor Kontrol S4 , Traktor Kontrol S5 , Traktor Kontrol S8 , Traktor Kontrol Z2</li>
          <li>Allen&Heath XONE:DX</li>
          <li>Vestax : VCI-100 , VCI-100 mk2 , VCI-300 , VCI-300 mk2 , VCI-380 ,VCI-400 , VCI-400 TR , VCI-400 DJ , VCM-100 , VCM-600 , Typhoon , Typhoon VDJ , Spin , Spin 2 , PBS-4</li>
          <li>Numark : NV , NS 6 , NS 7 , N4 , Mixtrack Pro Dj , Mixtrack Pro II , Mixtrack Pro III , Mixtrack Platinum , Mixtrack Quad , DJ2GO , 4TRAK , Mixtrack Edge , iDJ , iDJ II iDJ live II , Party Mix , Total Control , Stelth Control , Omni Control , MixMeister Control</li>
          <li>Denon : MC4000 , MC6000</li>
          <li>Ableton : PUSH , PUSH 2</li>
          <li>Reloop : Beatmix 2 , Beatmix 4</li>
          <li>Stanton : DJC.4 , SCS.4DJ , SC System 3</li>
          <li>M-Audio : Torg Xponent , Trigger Finger Pro NOVATION LAUNCH CONTROL , NOVATION LAUNCH CONTROL Mini , ROLAND DJ-808 и др.</li>
        </ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbk-mini"=>'<h1>Сумка-рюкзак DJB K-mini Plus предназначен для переноски и хранения DJ контроллеров.</h1>
        <h2>Видео:</h2>
        <iframe style="display:block;" width="100%" height="400px" src="https://www.youtube.com/embed/nxWoZp8xOrs" frameborder="0" allowfullscreen></iframe>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON300D;</li>
			<li>Размеры контроллера до 58х40х9 см;</li>
			<li>Вес: 1.28 кг;</li>
			<li>Стенки сумки имеют пластиковый каркас с мягким наполнением которые надежно защищают контроллер от внешних воздействий;</li>
			<li>Эти модели имеют удобные ручки для транспортировки, но и специальные лямки, что бы использовать сумку как рюкзак, которые легко убираются в специальный карман;</li>
			<li>Три наружных кармана на молнии: один большой карман с внутренним карманом сеточкой в который без проблем поместятся наушники, коммутация или что то крайне необходимое, и два кармана для не больших предметов;</li>
			<li>Внутри сумка имеет большой мягкий карман для ноутбука с диагональю экрана до 17 дюймов, один не большой карман на молнии для документов или проводов;</li>
			<li>Крышку сумки от контроллера внутри разделяет мягкий мат с двумя  карманами на молнии под размер листа А4 каждый;</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DDJ : RB , RR , RX, SB2 , SR , SP1 ,WEGO , а также TORAIZ TSP-16, XDJ-AERO , XDJ-R1</li>
          <li>NATIVE INSTRUMENTS : Traktor Kontrol S2, Traktor Kontrol Z2</li>
          <li>Allen&Heath XONE:DX</li>
          <li>Vestax : VCI-100 , VCI-100 mk2 , VCI-300 , VCI-300 mk2 , VCI-380 , VCM-100 , VCM-600 , Typhoon , Typhoon VDJ , Spin , Spin 2 , PBS-4</li>
          <li>Numark : Mixtrack Pro Dj , Mixtrack Pro II , Mixtrack Pro III , Mixtrack Platinum , Mixtrack Quad , DJ2GO , Mixtrack Edge , iDJ , iDJ II,  iDJ live II , Party Mix , Total Control , Stelth Control , Omni Control , MixMeister Control</li>
          <li>Denon : MC4000 , MC6000</li>
          <li>Ableton : PUSH , PUSH 2</li>
          <li>Reloop : Beatmix 2 , Beatmix 4</li>
          <li>Stanton : DJC.4 , SCS.4DJ , SC System 3</li>
          <li>M-Audio : Torg Xponent , Trigger Finger Pro LAUNCH CONTROL , NOVATION LAUNCH CONTROL Mini и др</li>
        </ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>',
    "djbhp"=>'<h1>Сумка DJB HP предназначена для переноски и хранения наушников.</h1>
        <h2>Видео:</h2>
        <iframe style="display:block;" width="100%" height="400px" src="https://www.youtube.com/embed/3QVFoZ02JVo" frameborder="0" allowfullscreen></iframe>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON300D;</li>
			<li>Размеры наушников до 21х18х9/6 см;</li>
			<li>Вес 250 гр;</li>
			<li>Имеет ручку и плечевой ремень;</li>
			<li>Наружный карман с молнией и с выходом провода для наушников;</li>
			<li>Два внутренних кармана сеточки, один на молнии для хранения не больших предметов; (переходники, флешь карты и т.д.), второй карман по больше, без молнии, для принадлежностей, к которым нужен быстрый доступ;</li>
			<li>Мягкие стенки сумки надежно защитят ваши наушники от внешних воздействий;</li>
		</ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbcdm"=>'<h1>Сумка-рюкзак для переноски и хранения DJ микшерных пультов и проигрывателей компакт дисков.</h1>
        <h2>Видео:</h2>
        <iframe style="display:block;" width="100%" height="400px" src="https://www.youtube.com/embed/IqegWV0GlGs" frameborder="0" allowfullscreen></iframe>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Эти модели имеют удобные ручки для транспортировки, но и специальные лямки, что бы использовать сумку как рюкзак, которые легко убираются в специальный карман.</li>
			<li>Большой наружный карман на молнии с дополнительными отделениями под ноутбук или пластинки, сетчатый карман на молнии.</li>
			<li>Стенки модели имеют пластиковый каркас с мягким наполнением которые надежно защищают контроллер от внешних воздействий, так же для удобства используются мягкие вставки для подгонки вашего устройства под сумку.</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON300D;</li>
			<li>Размеры контроллера до 340х420х140 см.;</li>
			<li>Вес 1,4 кг.;</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DJM – 500, 600, 750, 800, 850, 900 – х серий.</li>
          <li>Pioneer СDJ -  800, 850, 900, 1000, 2000 – х серий.</li>
        </ul>
        <p>Универсальная сумка рюкзак подходит под большое количество музыкального и DJ оборудования, для уточнения совместимости сравните размер вашего изделия c размерами сумки-рюкзака DJB CD&M.</p>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbv"=>'<h1>СУМКА ДЛЯ ВИНИЛОВЫХ ПРОИГРЫВАТЕЛЕЙ С ПЛЕЧЕВЫМ РЕМНЕМ</h1>
        <h2>Видео:</h2>
        <iframe style="display:block;" width="100%" height="400px" src="https://www.youtube.com/embed/bczfCSYKcyc" frameborder="0" allowfullscreen></iframe>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON300D;</li>
			<li>Размеры контроллера до 480х400х180 см;</li>
			<li>Вес:2.9 кг.;</li>
			<li>Эта модель имеет удобные ручки для транспортировки и плечевой ремень;</li>
			<li>Три наружных кармана на молнии и один на «липучке»: один большой карман на лицевой стороне сумки, два боковых кармана для не больших предметов, и большой задний карман на липучке;</li>
			<li>Внутри сумка имеет большой прозрачный карман на молнии для документов или проводов;</li>
			<li>Защиту проигрывателя, а в особенности платтера (диска) от выпадения, обеспечивает специальный мягкий мат, который плотно прижимается к проигрывателю толстыми липучими лентами, что позволяет проигрывателю быть надежно прижатым к сумке;</li>
			<li>Стенки сумки имеют пластиковый каркас с мягким наполнением в 15-20 мм которые надежно защищают проигрыватель от внешних воздействий;</li>
		</ul>
        <h2>Совместимость сумок с проигрывателями следующий моделей:</h2>
        <ul>
          <li>Technics SL-1200, SL-1200 MK2, SL-1200GR, SL-1210, SL-1210MK2 и т.д.</li>
          <li>Vestax PDX-3000 Mk2</li>
          <li>Pioneer PLX – 500, PLX – 1000</li>
          <li>Audio-Technica AT-LP1240USB,  AT LP60USB</li>
          <li>Numark TT250</li>
          <li>Reloop RP-1000 M, RP-2000 MK3</li>
        </ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "backpack"=>'<h1>DJB Backpack рюкзак для микшерных пультов и проигрывателей</h1>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON600D;</li>
			<li>Эти модели имеют надежные колесики на подшипниках и удобные ручки для переноса;</li>
			<li>Наружный карман для мелочей;</li>
			<li>Внутренний отсек разделен на секции для удобства;</li>
			<li>Имеются дополнительные липучки для крепления контроллера к сумке.</li>
			<li>Мягкие стенки сумки надежно защитят оборудование;</li>
		</ul>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
    "djbcdmplus"=>'<h1>Сумка-рюкзак для переноски и хранения DJ микшерных пультов и проигрывателей компакт дисков.</h1>
        <h2>Видео:</h2>
        <iframe style="display:block;" width="100%" height="400px" src="https://www.youtube.com/embed/IqegWV0GlGs" frameborder="0" allowfullscreen></iframe>
        <br>
        <h3>Модель DJB CD&M Plus это улучшенная модель CD&M</h3>
        <h2>Основные изменения:</h2>
        <ul>
			<li>Мягкие широкие рюкзачные лямки;</li>
			<li>В стенки сумки по периметру основного отсека добавлен пластик;</li>
			<li>Новый плечевой ремень с мягким и широким наплечником;</li>
			<li>Карабины на лямках рюкзака и плечевого ремня заменены на надежные фастексы;</li>
			<li>На основных отсеках сумки мы стали использовать качественную фурнитуру японской компании YKK;</li>
			<li>В комплекте с сумкой теперь идет и сумка для аксессуаров или проводов DJA;</li>
			<li>Эти модели имеют удобные ручки для транспортировки, но и специальные лямки, что бы использовать сумку как рюкзак, которые легко убираются в специальный карман</li>
			<li>Большой наружный карман на молнии с дополнительными отделениями под ноутбук или пластинки, сетчатый карман на молнии</li>
			<li>Стенки модели имеют пластиковый каркас с мягким наполнением, которые надежно защищают контроллер от внешних воздействий, так же для удобства используются мягкие вставки для подгонки вашего устройства под сумку</li>
		</ul>
        <h2>Характеристики:</h2>
        <ul>
			<li>Цвет черный;</li>
			<li>Состав: NYLON1680D на основе ПВХ. подкладка NYLON300D;</li>
			<li>Размеры контроллера до 340х420х140 см;</li>
			<li>Вес: 1.4 кг$</li>
		</ul>
        <h2>Совместимость сумок с контроллерами следующий моделей:</h2>
        <ul>
          <li>Pioneer DJM – 500, 600, 750, 800, 850, 900 – х серий.</li>
          <li>Pioneer СDJ -  800, 850, 900, 1000, 2000 – х серий.</li>
        </ul>
        <p>Универсальная сумка рюкзак подходит под большое количество музыкального и DJ оборудования, для уточнения совместимости сравните размер вашего изделия c размерами сумки-рюкзака DJB CD&M Plus.</p>
        <h3>Заказ по телефону: 8 (800) 505-78-25</h3>
        <h3>Доставка по Москве бесплатная.</h3>
    ',
);
$key = $_POST['item'];
if (array_key_exists($key, $items)) {
    $result['data'] = $items[$key];
    $result['status'] = true;
}else{
    $result['status'] = false;
}
$result['info'] = $key;
echo json_encode($result);
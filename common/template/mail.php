<?php
/*
// массив получателей письма
$arrayTo = array(
'www.djbag.ru@gmail.com',
'sale@djbag.ru'
);
// переводим массив в строку
// и разделяем адреса запятыми
$strTo = implode(",", $arrayTo);
// Тема письма
$subject = "Заявка с сайта djbag.ru";
// Текст письма.
// Тут может быть как просто текст, так и html код
$message = '
<html>
<head>
<title>Тестовое письмо</title>
</head>
<body>
<p>Текст письма</p>
</body>
</html>
' ;
// заголовок письма
$headers= "MIME-Version: 1.0\r\n";
// кодировка письма
$headers .= "Content-type: text/html; charset=utf-8\r\n";
// от кого письмо
$headers .= "From: djBag <noreply@djbag.ru>\r\n";
// отправляем письмо
$result = mail($strTo, $subject, $message, $headers);
// результат отправки письма

header('Content-Type: application/json');
if($result){
	$arr = array("status"=>"ok");
} else{
	$arr = array("status"=>"fail");
}
echo json_encode($arr);
*/






//Емайл адрес на который будет отправляться
$to = 'www.djbag.ru@gmail.com, sale@djbag.ru';
$email = 'noreply@djbag.ru';
$req = $_POST;

$errCount =0;


if (empty($req['phone'])){
	$replyArr['errorText'][$errCount]='Напишите Ваш телефон.';
	$replyArr['errName'][$errCount]='phone';
	$errCount++;
}
if (empty($req['name'])){
	$replyArr['errorText'][$errCount]='Напишите Ваше имя.';
	$replyArr['errName'][$errCount]='name';
	$errCount++;
}
if (!empty($req['email'])){
	if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",trim($req['email']))){
		$replyArr["errorText"][$errCount]='email';
		$replyArr["errName"][$errCount]='Введите корректный емайл!';
		$errCount++;
	}
}


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "secret=6LcwICAUAAAAAGe1leoOA5pgxAEcEu7Xd9RYr98B&response=". $req['g-recaptcha-response'] ."");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);

$tovar = $req['tovar']?'Выбраный продукт: '. $req['tovar']:'';
$emem = $req['email']?'Емейл: '. $req['email']:'';
if($errCount==0 && json_decode($server_output)->success==true){
	$subject   = 'Заявка с сайта: '.$_SERVER['HTTP_HOST'];
	// текст письма
	
	$message_mail = '
	<html>
	<head>
	<title>'. $subject .'</title>
	</head>
	<body>
	<p>
		Телефон: '.$req['phone'].'<br>
		Имя: '.$req['name'].'<br>
		'.$emem.'<br>'.$tovar.'<br>
		Сообщение: '. $req['message'].'
	</p>
	</body>
	</html>
	';

	$message_mail = iconv("UTF-8", "KOI8-U", $message_mail);

	$subject = iconv("UTF-8", "KOI8-U", $subject);
	$subject = '=?koi8-r?B?'.base64_encode($subject).'?=';	

	$Uname = iconv("UTF-8", "KOI8-U", $req['name']);
	$Uname = '=?koi8-r?B?'.base64_encode($Uname).'?=';
	$from = $email;

	$headers  = "Content-type: text/html; charset=koi8-r \r\n"; 
	$headers .= "From: {$Uname} <{$from}>\r\n"; 
	//$headers .= "Reply-To: {$from}\r\n"; 

	
	if(mail($to, $subject, $message_mail, $headers)){
		$replyArr['status']='ok';
	}else{
		$replyArr['status']='fail';
	}

}else{
	$replyArr['statusre']=json_decode($server_output)->success;
	$replyArr['status']='fail';
}
header('Content-Type: application/json');
echo json_encode($replyArr);

?>